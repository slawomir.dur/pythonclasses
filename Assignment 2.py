class Human(object):
    def __init__(self, firstname, surname, sex):
        self.firstname = firstname
        self.surname = surname
        self.sex = sex
        print("A {} named {} {} is born.".format(self.sex, self.firstname, self.surname))

    def __del__(self):
        print("A {} named {} {} dies.".format(self.sex, self.firstname, self.surname))

class TruckDriver(Human):
    def __init__(self, firstname, surname, sex, truck):
        super(TruckDriver, self).__init__(firstname, surname, sex)
        self.truck = truck
        print("{} {} drives {}.".format(self.firstname, self.surname, self.truck))

    def DriveaTruck(self):
        print("{} {} is driving a truck.".format(self.firstname, self.surname))

    def __del__(self):
        print("{} {} has an accident and dies.".format(self.firstname, self.surname))

class Plumber(Human):
    def MendPipes(self):
        print("{} {} is mending pipes.".format(self.firstname, self.surname))

class Official(Human):
    def __init__(self, firstname, surname, sex, office_id):
        super(Official, self).__init__(firstname, surname, sex)
        self.office_id = office_id


jan = TruckDriver("Jan", "Kowalski", "Man", "Scania")
jan.DriveaTruck()
del jan

mscislaw = Plumber("Mscislaw", "Otwocki", "Man")
mscislaw.MendPipes()