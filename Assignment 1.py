class Muzyka(object):
    pass

class Instrumenty(Muzyka):
    pass

class Strunowe(Instrumenty):
    pass

class Smyczkowe(Strunowe):
    pass

class Szarpane(Strunowe):
    pass

class Mloteczkowe(Strunowe):
    pass

class Dete(Instrumenty):
    pass

class DeteBlaszane(Dete):
    pass

class DeteDrewniane(Dete):
    pass

class Perkusyjne(Instrumenty):
    pass

class Membranofony(Perkusyjne):
    pass

class Idiofony(Perkusyjne):
    pass

saksofon = DeteDrewniane()
puzon = DeteBlaszane()
gitara = Szarpane()
skrzypce = Smyczkowe()
fortepian = Mloteczkowe()
bebny = Perkusyjne()



print(isinstance(saksofon, DeteDrewniane))
print(isinstance(puzon, DeteBlaszane))
print(isinstance(gitara, Szarpane))
print(isinstance(skrzypce, Smyczkowe))
print(isinstance(fortepian, Mloteczkowe))
print(isinstance(bebny, Perkusyjne))
print(isinstance(skrzypce, Perkusyjne))

